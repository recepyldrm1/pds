## PDS

This is a readme file for our repository for the project in Data Science.

* Source code contains all source codes for creation of new datasets, visualisations, in a jupyter notebook where possible.
* Data contains the raw and intermediate datasets, in csv format.
* Figures & Visualisations contain all figures and visualisations created.
* References contains background story and other explanatory materials

Google document with discussion per question and report: and https://docs.google.com/document/d/1haPy7mAaKKsXw8-Kq2lawTIlVphBGGgmkXbqDHd2fSo/edit

Miro board with visual representations of findings: https://miro.com/app/board/o9J_lqsTfvQ=/

